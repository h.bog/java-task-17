package shopping;

public class Product{

	private String name = "";
	private float unitPrice = 0;

	public Product(){
		
	}

	public Product(String name, float unitPrice){
		this.name = name;
		this.unitPrice = unitPrice;
	}	

	public String getName(){
		return this.name;
	}

	public float getUnitPrice(){
		return this.unitPrice;
	}


}