package shopping;

import java.util.ArrayList;

public class ProductList{

	ArrayList<Product> list = new ArrayList<Product>();

	public void add(Product item){
		list.add(item);
	}

	public Product get(String name){
		for (Product p : this.list){
			if (p.getName().equals(name)){
				return p;
			}
		} 

		return new Product("nan", 0);

	}

	public void printList(){
		System.out.println("Welcome to the best shop ever!");
		System.out.println("---------------------------"
							+"\nAvailable products:");
		for (Product elem : this.list){
			System.out.format("%8s", elem.getName());
			System.out.format("%8s", elem.getUnitPrice());
			System.out.println();
		}
		System.out.println("---------------------------");
	}


}