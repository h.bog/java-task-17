package shopping;

import java.util.ArrayList;

public class CardDatabase {
	private ArrayList<Card> cards = new ArrayList<Card>();

	public CardDatabase(){

	}
	
	public void add(Card c){
		cards.add(c);
	}

	public boolean authorizationApproved(Card currentCard, String enteredPin){
		for (Card c : cards){
			if (c.getNumber().equals(currentCard.getNumber())){
				if (c.getPin().equals(enteredPin)){
					return true;
				}
			}
		}
		return false;
	}

}