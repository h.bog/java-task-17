package shopping;

public class CreditCard extends Card{
	private float limit;
	private float amountUsed;

	public CreditCard(){
		super();
		limit = 0;
		amountUsed = 0;
		
	}

	public CreditCard(String userName, String number, String pin, float limit, float amountUsed){
		super(userName, number, pin);
		this.limit = limit;
		this.amountUsed = amountUsed;
		
	}

	public float getLimit(){
		return this.limit;
	}

	public float getAmountUsed(){
		return this.amountUsed;
	}

	public void updateAmountUsed(float newValue){
		this.amountUsed = newValue;
	}
}