package shopping;

public class DebitCard extends Card{
	private float saldo;

	public DebitCard(){
		super();
		saldo = 0;
	}
	public DebitCard(String userName, String number, String pin, float saldo){
		super(userName, number, pin);
		this.saldo = saldo;
	}

	public float getSaldo(){
		return this.saldo;
	}

	public void setSaldo(float number){
		this.saldo = number;
	}
}