import shopping.*;

import java.util.ArrayList;
import java.util.Scanner;


public class program{
	public static void main(String[] args){

		Scanner input = new Scanner(System.in);
		Cart cart = new Cart();
		boolean cartIsDone = false;

		CardDatabase cards = new CardDatabase(); 
		cards.add(new Card("Tom Warren", "4567 2332 5567 1298", "4035"));
		cards.add(new Card("Ben James", "1234 5678 1234 5678", "8899"));
		cards.add(new Card("Rose Howells", "9495 3820 3033 2156", "9876"));

		ProductList products = new ProductList();
		products.add(new Product("coke", 26));
		products.add(new Product("orange", 10));
		products.add(new Product("pasta", 15));
		products.add(new Product("milk", 23));
		products.add(new Product("salmon", 79));

		products.printList();

		while (!cartIsDone){
			System.out.println("Type an item from the list above to add it to your cart.");
			cart.add(products.get(input.nextLine()));
			System.out.println("Do you want to add another item? y/n");
			String cont = input.nextLine().toLowerCase();
			if (cont.equals("n")){
				cartIsDone = true;
			} else{
				continue;
			}
		}


		cart.printCart();
		float total = cart.calculateValue();
		System.out.println("Amount to pay: "+ total + " kr."
							+ "\nHow would you like to pay? card / cash");
		String paymentMethod = input.nextLine().toLowerCase();
		Boolean paymentMethodAccepted = false;

		while (!paymentMethodAccepted){
			switch(paymentMethod){
				case "cash":
					paymentMethodAccepted = true;
					System.out.println("How much cash do you want to pay with?");
					String amount = input.nextLine();

					Boolean cashAccepted = false;
					
					
					while (!cashAccepted) {
						Float n = Float.parseFloat(amount);
						Cash cash = new Cash(n);
						if (cash.getSaldo() > total){
							cashAccepted = true;
							System.out.println("\n~~~~~~~~~Thank you for purchasing!~~~~~~~~~\n");
							cash.setSaldo(cash.getSaldo()-total);
							System.out.println("You get back: " + cash.getSaldo() + "kr.");

						} else {
							System.out.println("Not enough money given, give a new amount:");
							amount = input.nextLine();
						}	
					}
					break;

				case "card":
					paymentMethodAccepted = true;
					System.out.println("Debit or Credit card? debit/credit");
					String cardType = input.nextLine().toLowerCase();
					Boolean cardTypeAccepted = false;

					while (!cardTypeAccepted){
						switch (cardType){
							case "credit":
								cardTypeAccepted = true;
								CreditCard currentCreditCard = new CreditCard("Ben James", "1234 5678 1234 5678", "8899", 5000, 2390);
								System.out.println("Please enter pin: (Hint: 8899)");
								String enteredPin = input.nextLine();

								Boolean pinCreditCorrect = false;

								while (!pinCreditCorrect) {
									if (cards.authorizationApproved(currentCreditCard, enteredPin)){
										pinCreditCorrect = true;
										System.out.println("Pin correct!");
										if (total < (currentCreditCard.getLimit()-currentCreditCard.getAmountUsed())){
											System.out.println("\n~~~~~~~~~Thank you for purchasing!~~~~~~~~~\n");
											System.out.println("Available credit before purchase: " + (currentCreditCard.getLimit()-currentCreditCard.getAmountUsed()) + "kr.");
											currentCreditCard.updateAmountUsed(currentCreditCard.getAmountUsed()+total);
											System.out.println("Available credit after purchase: " + (currentCreditCard.getLimit()-currentCreditCard.getAmountUsed()) + "kr.");

										} else {
											System.out.println("Not enough money.\n"+
																"You have: " + (currentCreditCard.getLimit()-currentCreditCard.getAmountUsed()) + "kr left to spend on your credit card.");

										}
									} else{
										System.out.println("Pin incorrect, try again. (Hint: 8899)");
										enteredPin = input.nextLine();
									}
								}
								break;
							case "debit":
								cardTypeAccepted = true;
								DebitCard currentDebitCard = new DebitCard("Tom Warren", "4567 2332 5567 1298", "4035", 100);
								System.out.println("Please enter pin: (Hint: 4035)");
								enteredPin = input.nextLine();

								Boolean pinDebitCorrect = false;

								while (!pinDebitCorrect){
									if (cards.authorizationApproved(currentDebitCard, enteredPin)){
										pinDebitCorrect = true;
										System.out.println("Pin correct!");
										if (total < (currentDebitCard.getSaldo())){
											System.out.println("\n~~~~~~~~~Thank you for purchasing!~~~~~~~~~\n");
											System.out.println("Balance before purchase: "+ currentDebitCard.getSaldo());
											currentDebitCard.setSaldo(currentDebitCard.getSaldo()-total);
											System.out.println("Balance after purchase: " + currentDebitCard.getSaldo());
										} else {
											System.out.println("Not enough money.\n"+
																"You have: " + currentDebitCard.getSaldo() + "kr left to spend on your credit card.");

										}
									} else{
										System.out.println("Pin incorrect, try again. (Hint: 4035)");
										enteredPin = input.nextLine();
									}
								}
								break;
							default:
								System.out.println("Debit or Credit card? debit/credit");
								cardType = input.nextLine().toLowerCase();
						}
					}

					break;
				default:
					System.out.println("Amount to pay: "+ total + " kr."
										+ "\nHow would you like to pay? card/cash");
					paymentMethod = input.nextLine().toLowerCase();

			}
		}
	}
}

