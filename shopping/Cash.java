package shopping;

public class Cash{
	private float saldo;

	public Cash(){
		saldo = 0;
	}

	public Cash(float saldo){
		this.saldo = saldo;
	}

	public float getSaldo(){
		return saldo;
	}

	public void setSaldo(float newSaldo){
		saldo = newSaldo;
	}
}