package shopping;

import java.util.ArrayList;

public class Cart{

	private ArrayList<Product> cart = new ArrayList<Product>();
	private float value = 0;

	public Cart(){

	}

	public void add(Product p){
		if (p.getName().equals("nan")){
			System.out.println("Not in product list.");
		} else{
			cart.add(p);
			System.out.println("- " + p.getName() + " have been added to cart");
		}
	}

	public void printCart(){
		System.out.println("..........................."+
							"\nThis is your cart:");
		for (Product item : cart){
			System.out.println(item.getName());
		}
		System.out.println("...........................");
	}

	public float calculateValue(){
		for (Product item : cart){
			value += item.getUnitPrice();
		}
		return value;
	}
}