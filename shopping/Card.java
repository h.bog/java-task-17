package shopping;

public class Card{
	private String userName = "";
	private String number = "";
	private String pin = "";

	public Card(){ 
	}

	public Card(String userName, String number, String pin){
		this.userName = userName;
		this.number = number;
		this.pin = pin;
	}

	public String getUserName(){
		return this.userName;
	}

	public String getNumber(){
		return this.number;
	}

	public String getPin(){
		return this.pin;
	}
}